package com.learn.recipe.models;

import javax.persistence.*;

@Entity
@Table(name = "recipes")
public class Recipe {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(name = "name")
  private String name;

  @Column(name = "description")
  private String description;

  @Column(name = "mealType")
  private String mealType;

  @Column(name = "dishType")
  private String dishType;

  @Column(name = "cookingTime")
  private String cookingTime;

  public Recipe() {

  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getMealType() {
    return mealType;
  }

  public void setMealType(String mealType) {
    this.mealType = mealType;
  }

  public String getDishType() {
    return dishType;
  }

  public void setDishType(String dishType) {
    this.dishType = dishType;
  }

  public String getCookingTime() {
    return cookingTime;
  }

  public void setCookingTime(String cookingTime) {
    this.cookingTime = cookingTime;
  }

  @Override
  public String toString() {
    return "Tutorial [id = " + id + ", name = " + name + ", description = " + description + ", mealType = " + mealType
        + ", dishType = " + dishType + ", cookingTime = " + cookingTime + "]";
  }
}