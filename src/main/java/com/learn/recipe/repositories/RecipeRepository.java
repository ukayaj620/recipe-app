package com.learn.recipe.repositories;

import java.util.List;

import com.learn.recipe.models.Recipe;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findByName(String name);
}
