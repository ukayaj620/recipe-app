package com.learn.recipe.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.learn.recipe.models.Recipe;
import com.learn.recipe.repositories.RecipeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/recipe")
public class RecipeController {

	@Autowired
	RecipeRepository recipeRepository;

	@GetMapping("/list")
	public String getAll(@RequestParam(required = false) String name, Model model) {
		List<Recipe> recipes = new ArrayList<Recipe>();
		if (name == null)
			recipeRepository.findAll().forEach(recipes::add);
		else
			recipeRepository.findByName(name).forEach(recipes::add);

		if (recipes.isEmpty()) {
			model.addAttribute("recipes", null);
		}
		model.addAttribute("recipes", recipes);
		return "index";
	}

	@GetMapping("/create")
	public String showCreateRecipeForm(Recipe recipe) {
		return "recipe/create";
	}

	@PostMapping("/create")
	public String createRecipe(@Valid Recipe recipe, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "recipe/create";
		}
		recipeRepository.save(recipe);
		return "redirect:/recipe/list";
	}
}
